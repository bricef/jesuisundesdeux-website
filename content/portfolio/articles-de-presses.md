---
title: "Articles de presses"
type: portfolio
date: 2018-12-01T12:51:00
submitDate: 25 Octobre 2018
caption: Dans la presse
image: images/portfolio/presse.jpg
category: ["information"]
client: Organe de presse

---
### La presse en parle

#### Temps reels

  - [**Twitter**: temps réel](https://twitter.com/hashtag/jesuisundesdeux?f=tweets)

#### Novembre 2018

  -  [**Midilibre**:"Je suis un des deux", la réponse des cyclistes à Philippe Saurel](https://www.midilibre.fr/2018/11/10/montpellier-je-suis-un-des-deux-la-reponse-des-cyclistes-a-philippe-saurel,4830605.php)
  - [**Midilibre**:Ils n'étaient pas 2, mais 1 200 à manifester à vélo](https://www.midilibre.fr/2018/11/10/montpellier-ils-netaient-pas-2-mais-1200-a-manifester-a-velo,4830666.php)
  - [**Mediapart**: #JeSuisUnDesDeux, ou comment le mépris a uni les vélotafeurs de Montpellier](https://blogs.mediapart.fr/brice-favre/blog/281018/jesuisundesdeux-ou-comment-le-mepris-uni-les-vélotafeurs-de-montpellier)
  - [**France 3**: Les cyclistes #JeSuisUnDesDeux étaient plus d'un millier battre le pavé en réponse à Philippe Saurel](https://france3-regions.francetvinfo.fr/occitanie/herault/montpellier/montpellier-cyclistes-jesuisundesdeux-etaient-plus-millier-battre-pave-reponse-philippe-saurel-1572752.html)
  - [**e-metropolitain**: En images / Montpellier : #Jesuisundesdeux rassemble 1 200 cyclistes](https://e-metropolitain.fr/2018/11/10/en-images-montpellier-jesuisundesdeux-rassemble-1-200-cyclistes/)
  - [**viaoccitanie**: #jesuisundesdeux : Les cyclistes mobilisés pour dénoncer le mauvais aménagement de Montpellier](https://viaoccitanie.tv/jesuisundesdeux-les-cyclistes-mobilises-pour-denoncer-le-mauvais-amenagement-de-montpellier/)
  - [**lemouvement**: Montpellier : manifestation cycliste #jesuisundesdeux, samedi 10 novembre](https://lemouvement.info/2018/11/08/montpellier-manifestation-cycliste-jesuisundesdeux-samedi-10-novembre/)
  - [**lemouvement**: Montpellier, manif citoyenne 1200 cyclistes en colère](https://lemouvement.info/2018/11/11/montpellier-manif-citoyenne-1200-cyclistes-en-colere/)
  - [**lagglorieuse**: #JeSuisUndesDeux](http://www.lagglorieuse.info/article_jesuisundesdeux.html)
  - [**20minutes**: On risque sa vie à bicyclette», près de 1.500 cyclistes réclament plus de sécurité](https://www.20minutes.fr/montpellier/2369479-20181111-video-montpellier-risque-vie-bicyclette-pres-1500-cyclistes-reclament-plus-securite)
  - [**Montpellier-metropole**: Quand le hashtag #JeSuisUnDesDeux des adeptes du vélo devient viral](http://www.montpellier-metropole.com/montpellier-quand-le-hashtag-jesuisundesdeux-des-adeptes-du-velo-devient-viral/)
  - [**francebleu**: Plus de 1.000 cyclistes dans les rues de Montpellier pour défendre la place du vélo dans la ville](https://www.francebleu.fr/infos/societe/plus-de-1000-cyclistes-dans-les-rues-de-montpellier-pour-defendre-la-place-du-velo-dans-la-ville-1541857423)
  - [**francoisbaraize**: La politique d’élimination des cyclistes](http://francoisbaraize.fr/la-politique-delimination-des-cyclistes/)


#### Octobre 2018

 - [**Midilibre**: Quand le hashtag #JeSuisUnDesDeux des adeptes du vélo devient viral](https://www.midilibre.fr/2018/10/25/montpellier-quand-le-hashtag-jesuisundesdeux-des-adeptes-du-velo-devient-viral,4745201.php)